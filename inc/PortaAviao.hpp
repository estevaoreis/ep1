#ifndef PORTAAVIAO_HPP
#define PORTAAVIAO_HPP
#include<string>
#include<iostream>
#include "barco.hpp"

using namespace std;
class PortaAviao: public Barco {
    private:   
        int quantidade_defesa;
        
    public:
        // Construtor e Destrutor
        PortaAviao();
        PortaAviao(int coordenadaX, int coordenadaY, char orientacao, bool derrotado, int quantidade_defesa);
        ~PortaAviao();
        
        // Métodos Acessores
        int get_quantidade_defesa();
        void set_quantidade_defesa(int quantidade_defesa);
        int verifica_defesa();

};

#endif