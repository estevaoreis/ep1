#ifndef BARCO_HPP
#define BARCO_HPP
#include<iostream>
#include<string>

using namespace std;

class Barco{
	private:
		int identificador;
		int coordenadaX;
		int coordenadaY;
		int quantidade_casas;
		char orientacao;
		int resistividade;
		bool derrotado;
		
	public:
		// Construtor e Destrutor
		Barco();
		Barco(int identidade,int coordenadaX, int coordenadaY, char orientacao, int quantidade_casas, bool derrotado);
		~Barco();
		// Métodos Acessores
		int get_identificador();
		void set_identificador(int identificador);

		int get_coordenadaX();
		void set_coordenadaX(int coordenadaX);
		
		int get_coordenadaY();
		void set_coordenadaY(int coordenadaY);

		int get_quantidade_casas();
		int set_quantidade_casas(int quantidade_casas);

		char get_orientacao();
		void set_orientacao(char orientacao);

		int get_resistividade();
		void set_resistividade(int resistividade);
		//void verifica_resistividade(Barco **barco);

		bool get_derrotado();
		void set_derrotado(bool derrotado);
};

#endif

