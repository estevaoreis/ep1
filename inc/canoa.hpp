#ifndef CANOA_HPP
#define CANO_HPP
#include<string>
#include<iostream>
#include "barco.hpp"

using namespace std;
class Canoa: public Barco {
    private:
        int resistividade;
    public:
        // Construtor e DEstrutor
        Canoa();
        Canoa(int coordenadaX, int coordenadaY, char orientacao, bool derrotado);
        ~Canoa();

};

#endif