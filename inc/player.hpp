#ifndef PLAYER_HPP
#define PLAYER_HPP
#include<iostream>
#include<string>
#include "barco.hpp"
#include "PortaAviao.hpp"
#include "canoa.hpp"
#include "submarino.hpp"

using namespace std;
class Player {
    private:
        string nome;
        int vidas;
        bool vitoria;
    public:
        Player(string nome, int vidas);
        ~Player();

        string get_nome();
        void set_nome(string nome);

        int get_vida();
        void set_vida(int vidas);

        bool get_vitoria();
        void set_vitoria(bool vidas);




};
#endif