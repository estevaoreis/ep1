#ifndef SUBMARINO_HPP
#define SUBMARINO_HPP
#include<string>
#include<iostream>
#include "barco.hpp"

using namespace std;
class Submarino: public Barco{
    private:
        
    public:
        //Construtor e Destrutor
        Submarino();
        Submarino(int coordenadaX, int coordenadaY, char orientacao, bool derrotado);
        ~Submarino();
        // Métodos Acessore
        //void verifica_resistividade(Submarino **submarino);
};
#endif

