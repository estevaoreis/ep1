#ifndef TABULEIRO_HPP
#define TABULEIRO_HPP
#include<iostream>
#include<string>
#include<vector>
#include "barco.hpp"
#include "PortaAviao.hpp"
#include "canoa.hpp"
#include "submarino.hpp"
#include "player.hpp"

using namespace std;
class Tabuleiro{
    public:
        int quantidade_linhas;
        int quantidade_colunas;
        string tabuleiro_grafico[13][13];
        int tabuleiro_real[13][13];

        Tabuleiro();
        ~Tabuleiro();

        
        void desenha_tabuleiro_grafico(string **tabuleiro_grafico);
        void atualiza_tabuleiro_grafico(int **tabuleiro_real, int linha, int coluna);
        void inicializa_tabuleiro_real();
        void coloca_barcos_tabuleiro_real(int linha, int coluna,int **tabuleiro_real, int identificador, char orientacao);
        void altera_tabuleiro_real(int linha, int coluna,int **tabuleiro_real);
        
};

#endif