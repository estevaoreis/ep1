#include<iostream>
#include<string>
#include<cstdlib>
#include<ctime>
#include "PortaAviao.hpp"
#include "barco.hpp"

PortaAviao::PortaAviao(){
   
}
 PortaAviao::PortaAviao(int coordenadaX, int coordenadaY, char orientacao, bool derrotado, int quantidade_defesa){
    set_identificador(2);
	set_coordenadaX(coordenadaX);
	set_coordenadaY(coordenadaY);
    set_orientacao(orientacao);
	set_quantidade_casas(4);
	set_derrotado(derrotado);
    set_quantidade_defesa(quantidade_defesa);
    
 }
PortaAviao::~PortaAviao(){
    

}

int PortaAviao::get_quantidade_defesa(){
    return quantidade_defesa;
}

void PortaAviao::set_quantidade_defesa(int quantidade_defesa){
    this->quantidade_defesa = quantidade_defesa;
}

int PortaAviao::verifica_defesa(){
    int numero_aleatorio=0, new_defesa=0, quantidade_defesa=0;
    srand(time(NULL));
    quantidade_defesa = get_quantidade_defesa();
    numero_aleatorio = 1+(rand()%10);
    if(numero_aleatorio>=1 && numero_aleatorio <= 5 && quantidade_defesa >=1){
            new_defesa = quantidade_defesa--;
            set_quantidade_defesa(new_defesa);
            cout<<"O Porta Aviao defendeu!!"<<endl;
            return 0;
    }
    return 1;

}

