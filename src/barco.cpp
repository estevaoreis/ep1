#include<string>
#include<iostream>
#include "barco.hpp"

Barco::Barco(){
	
}
Barco::Barco(int identidade,int coordenadaX, int coordenadaY, char orientacao, int quantidade_casas, bool derrotado){
	set_identificador(identidade);
	set_coordenadaX(coordenadaX);
	set_coordenadaY(coordenadaY);
	set_orientacao(orientacao);
	set_quantidade_casas(quantidade_casas);
	set_derrotado(derrotado);
}

Barco::~Barco(){
	
}

int Barco::get_identificador(){
	return identificador;
}

void Barco::set_identificador(int  identificador){
	this->identificador = identificador;
}

int Barco::get_coordenadaX(){
	return coordenadaX;
}
void Barco::set_coordenadaX(int coordenadaX){
	this->coordenadaX = coordenadaX;
}
int Barco::get_coordenadaY(){
	return coordenadaY;
}
void Barco::set_coordenadaY(int coordenadaY){
	this->coordenadaY = coordenadaY;
}

int Barco::get_quantidade_casas(){
	return quantidade_casas;
}
int Barco::set_quantidade_casas(int quantidade_casas){
	this->quantidade_casas = quantidade_casas;
}

char Barco::get_orientacao(){
	return orientacao;
}
void Barco::set_orientacao(char orientacao){
    this->orientacao = orientacao;
}
int Barco::get_resistividade(){
	return resistividade;
}

void Barco::set_resistividade(int resistividade){
	this-> resistividade = resistividade;
}

/*void Barco::verifica_resistividade(Barco **barco){
		int resistividade;
		if(barco == NULL){
			resistividade = get_resistividade()-1;
			set_resistividade(resistividade);
		}
}
*/
bool Barco::get_derrotado(){
	return derrotado;
}
void Barco::set_derrotado(bool derrotaod){
	this-> derrotado = derrotado;
}
