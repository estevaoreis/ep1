#include<iostream>
#include<string>
#include "tabuleiro.hpp"
#include<vector>
#include "barco.hpp"
#include "PortaAviao.hpp"
#include "submarino.hpp"
#include "canoa.hpp"

Tabuleiro::Tabuleiro(){

}
Tabuleiro::~Tabuleiro(){

}
void Tabuleiro::desenha_tabuleiro_grafico(string **tabuleiro_grafico){
    int i, j;
    cout<<endl;
    
     cout<<"  -------------------------------------------"<<endl;
    cout<<"  |  0  1  2  3  4  5  6  7  8  9 10 11 12"<<endl;
    cout<<"  +------------------------------------------"<< endl;
    
    for(i= 0; i<13; i++){
        for(j=0; j<13; j++){
            tabuleiro_grafico[i][j] = "  .";
        }
    }
    //corpo[1][5]="  @";
     for(i= 0; i<13; i++){
         if(i>=0 && i<10){
            cout<<" "<< i<<'|';
        }
            else{
            cout << i<<'|';
        }
        for(j=0; j<13; j++){
            cout<<tabuleiro_grafico[i][j];
        }
    cout<<"  "<<"|"<<endl;
    }
    cout<<endl;

}

void Tabuleiro::atualiza_tabuleiro_grafico(int **tabuleiro_real, int linha, int coluna){
    cout<<endl;
    int i=0, j=0;
     cout<<"  -------------------------------------------"<<endl;
    cout<<"  |  0  1  2  3  4  5  6  7  8  9 10 11 12"<<endl;
    cout<<"  +------------------------------------------"<< endl;
    
    if(tabuleiro_real[linha][coluna]==1) tabuleiro_grafico[linha][coluna]="  C";
    else if(tabuleiro_real[i][j] ==2) tabuleiro_grafico[linha][coluna]="  P";
    else if(tabuleiro_real[i][j] ==4) tabuleiro_grafico[linha][coluna]="  S";
    else tabuleiro_grafico[linha][coluna]="  A";
     for(i= 0; i<13; i++){
         if(i>=0 && i<10){
            cout<<" "<< i<<'|';
        }
            else{
            cout << i<<'|';
        }
        for(j=0; j<13; j++){
            cout<<tabuleiro_grafico[i][j];
        }
    cout<<"  "<<"|"<<endl;
    }
    cout<<endl;

}

void Tabuleiro::inicializa_tabuleiro_real(){
    int i= 0;
    int j=0;
    for(i; i<13; i++){
        for(j; j<13; j++){
        tabuleiro_real[i][j] = 0;
        }
    }

}

void coloca_barcos_tabuleiro_real(int linha, int coluna,int **tabuleiro_real, int identificador, char orientacao){
   int i=0, j=0;
    if(identificador == 1)tabuleiro_real[linha][coluna] = 1;
    else if(identificador == 2){
        if(orientacao == 'd'){
            for(coluna; coluna > coluna+4; coluna++)
            tabuleiro_real[linha][coluna] = 2;
        }
        else if(orientacao == 'e'){
            for(coluna; coluna > coluna-4; coluna--)
            tabuleiro_real[linha][coluna] = 2;
               
        }
        else if(orientacao == 'c'){
            for(linha; linha > linha-4; linha--)
            tabuleiro_real[linha][coluna] = 2;
               
        }
         else if(orientacao == 'b'){
            for(linha; linha > linha+4; linha++)
            tabuleiro_real[linha][coluna] = 2;
               
        }
    }
     else if(identificador == 3){
        if(orientacao == 'd'){
            for(coluna; coluna > coluna+4; coluna++)
            tabuleiro_real[linha][coluna] = 3;
        }
        else if(orientacao == 'e'){
            for(coluna; coluna > coluna-4; coluna--)
            tabuleiro_real[linha][coluna] = 3;
               
        }
        else if(orientacao == 'c'){
            for(linha; linha > linha-4; linha--)
            tabuleiro_real[linha][coluna] = 3;
               
        }
         else if(orientacao == 'b'){
            for(linha; linha > linha+4; linha++)
            tabuleiro_real[linha][coluna] = 3;
               
        }
    }
}

void altera_tabuleiro_real(int linha, int coluna, int **tabuleiro_real){
    if(tabuleiro_real[linha][coluna]== 1){
        tabuleiro_real[linha][coluna] = 9;
    }
        if(tabuleiro_real[linha][coluna]== 2){
        tabuleiro_real[linha][coluna] = 9;
    }

        if(tabuleiro_real[linha][coluna]== 3){
        tabuleiro_real[linha][coluna] = 4;
    }
        if(tabuleiro_real[linha][coluna]== 4){
        tabuleiro_real[linha][coluna] = 9;
    }
}
